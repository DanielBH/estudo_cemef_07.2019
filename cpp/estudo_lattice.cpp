#define __STDCPP_WANT_MATH_SPEC_FUNCS__ 1

#include <mpi.h>
#include "lammps.h"
#include "input.h"
#include <string.h>
#include <signal.h>
#include "signal_handling.h"
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ctype.h"
#include "unistd.h"
#include "sys/stat.h"
#include "input.h"
#include "style_command.h"
#include "universe.h"
#include "atom.h"
#include "atom_vec.h"
#include "comm.h"
#include "group.h"
#include "domain.h"
#include "output.h"
#include "thermo.h"
#include "force.h"
#include "pair.h"
#include "min.h"
#include "modify.h"
#include "compute.h"
#include "bond.h"
#include "angle.h"
#include "dihedral.h"
#include "improper.h"
#include "kspace.h"
#include "update.h"
#include "neighbor.h"
#include "special.h"
#include "variable.h"
#include "accelerator_cuda.h"
#include "error.h"
#include "memory.h"
#include "region.h"
#include "signal_handling.h"
#include "neigh_modify.h"
#include <iostream>


#ifdef LIGGGHTS_DEBUG
#include "fenv.h"
#endif

using namespace LAMMPS_NS;

template <typename T>
void command_creator(LAMMPS *lmp, int narg, char **arg)
{
  //for(int i = 0; i < narg; i++)
  //{
  //  std::cout << arg[i] <<  "*****************************" << std::endl;
  //}
  T cmd(lmp);
  cmd.command(narg,arg);
}

int main(int argc, char **argv)
{
  struct sigaction int_action, usr1_action, term_action;
  memset(&int_action, 0, sizeof(struct sigaction));
  memset(&usr1_action, 0, sizeof(struct sigaction));
  memset(&term_action, 0, sizeof(struct sigaction));

  int_action.sa_handler = SignalHandler::int_handler;
  sigaction(SIGINT, &int_action, NULL);
  // SIGTERM is handled the same way as sigint. Note that OpenMPI (and possibly other flavours)
  // convert a SIGINT to mpirun to a SIGTERM to its children. That's why we need to catch it too.
  sigaction(SIGTERM, &int_action, NULL);
  usr1_action.sa_handler = SignalHandler::usr1_handler;
  sigaction(SIGUSR1, &usr1_action, NULL);

  MPI_Init(&argc,&argv);

  LAMMPS *lammps = new LAMMPS(argc,argv,MPI_COMM_WORLD);

  Force *force = lammps->force;
  Domain *domain = lammps->domain;
  Modify *modify = lammps->modify;
  Atom *atom = lammps->atom;
  LAMMPS *lmp = lammps;
  Update *update = lammps->update;
  Comm *comm = lammps->comm;
  Neighbor *neighbor = lammps->neighbor;
  Output *output = lammps->output;
  Error *error = lammps->error;
  Group *group = lammps->group;

  // ************* Constantes ************* //
  char* diametro = "20e-3";
  char* densidade = "5400"; // [kg/m^3]
  char* domain_x_lo = "-1"; // [m]
  char* domain_x_hi = "+1"; // [m]
  char* domain_y_lo = "-1"; // [m]
  char* domain_y_hi = "+1"; // [m]
  char* domain_z_lo = "-1"; // [m]
  char* domain_z_hi = "+1"; // [m]

  int n_args; // Number of arguments
  char *cmd_args[2]; // Array to store arguments.

  // modify_timing on
  int timing = 1;
  modify->timing = timing;

  // dimension 3
  domain->dimension = force->inumeric(FLERR, "3");
  for (int i = 0; i < modify->ncompute; i++)
    modify->compute[i]->reset_extra_dof();

  // hard_particles yes"
  atom->get_properties()->do_allow_hard_particles();

  // atom_style granular
  n_args = 1;
  *cmd_args = new char[n_args];
  cmd_args[0] = "granular";
  atom->create_avec(cmd_args[0], n_args-1, &cmd_args[1], lmp->suffix);

  // atom_modify map array
  n_args = 2;
  *cmd_args = new char[n_args];
  cmd_args[0] = "map";
  cmd_args[1] = "array";
  atom->modify_params(n_args, cmd_args);

  // boundary f f f
  n_args = 3;
  *cmd_args = new char[n_args];
  cmd_args[0] = "f";
  cmd_args[1] = "f";
  cmd_args[2] = "f";
  domain->set_boundary(n_args, cmd_args, 0);

  // newton off
  force->newton_pair = 0;
  force->newton_bond = 0;
  force->newton = 0;

  // communicate single vel yes
  char *communicate[] = {"single", "vel", "yes"};
  comm->set(3, communicate);

  // units si
  update->set_units("si");

  // region domain block -1 +1 -1 +1 -1 +1 units box
  n_args = 10;
  *cmd_args = new char[n_args];
  cmd_args[0] = "domain";
  cmd_args[1] = "block";
  cmd_args[2] = domain_x_lo;
  cmd_args[3] = domain_x_hi; 
  cmd_args[4] = domain_y_lo; 
  cmd_args[5] = domain_y_hi; 
  cmd_args[6] = domain_z_lo; 
  cmd_args[7] = domain_z_hi; 
  cmd_args[8] = "units";
  cmd_args[9] = "box";
  domain->add_region(n_args, cmd_args);

  // create_box 3 domain
  n_args = 2;
  *cmd_args = new char[n_args];
  cmd_args[0] = "3";
  cmd_args[1] = "domain";
  command_creator<CreateBox>(lmp, n_args, cmd_args);

  //neighbor 20e-3 bin
  n_args = 2;
  *cmd_args = new char[n_args];
  cmd_args[0] = diametro;
  cmd_args[1] = "bin";
  neighbor->set(n_args, cmd_args);

  // neigh_modify delay 0
  n_args = 2;
  *cmd_args = new char[n_args];
  cmd_args[0] = "delay";
  cmd_args[1] = "0";
  command_creator<NeighModify>(lmp, n_args, cmd_args);

  // fix m1 all property/global youngsModulus peratomtype 5e6 210e9 210e9
  n_args = 8;
  *cmd_args = new char[n_args];
  cmd_args[0] = "m1";
  cmd_args[1] = "all";
  cmd_args[2] = "property/global";
  cmd_args[3] = "youngsModulus";
  cmd_args[4] = "peratomtype";
  cmd_args[5] = "5e6";
  cmd_args[6] = "210e9";
  cmd_args[7] = "210e9";
  modify->add_fix(n_args,cmd_args,lmp->suffix);

  // fix "m2" "all" "property/global" "poissonsRatio" "peratomtype" "0.3" "0.3" "0.3"
  n_args = 8;
  *cmd_args = new char[n_args];
  cmd_args[0]="m2";
  cmd_args[1]="all";
  cmd_args[2]="property/global";
  cmd_args[3]="poissonsRatio";
  cmd_args[4]="peratomtype";
  cmd_args[5]="0.3";
  cmd_args[6]="0.3";
  cmd_args[7]="0.3";
  modify->add_fix(n_args,cmd_args,lmp->suffix);
  
  // fix "m3" "all" "property/global" "coefficientRestitution" "peratomtypepair" "3" "0.5" "0.5" "0.5" "0.5" "0.5" "0.5" "0.5" "0.5" "0.5"
  n_args = 15;
  *cmd_args = new char[n_args];
  cmd_args[0]="m3";
  cmd_args[1]="all";
  cmd_args[2]="property/global";
  cmd_args[3]="coefficientRestitution";
  cmd_args[4]="peratomtypepair";
  cmd_args[5]="3";
  cmd_args[6]="0.5";
  cmd_args[7]="0.5";
  cmd_args[8]="0.5";
  cmd_args[9]="0.5";
  cmd_args[10]="0.5";
  cmd_args[11]="0.5";
  cmd_args[12]="0.5";
  cmd_args[13]="0.5";
  cmd_args[14]="0.5";
  modify->add_fix(n_args,cmd_args,lmp->suffix);

  // fix "m4" "all" "property/global" "coefficientFriction" "peratomtypepair" "3" "0.6" "0.6" "0.6" "0.6" "0.6" "0.6" "0.6" "0.6" "0.6"
  n_args = 15;
  *cmd_args = new char[n_args];
  cmd_args[0]="m4";
  cmd_args[1]="all";
  cmd_args[2]="property/global";
  cmd_args[3]="coefficientFriction";
  cmd_args[4]="peratomtypepair";
  cmd_args[5]="3";
  cmd_args[6]="0.6";
  cmd_args[7]="0.6";
  cmd_args[8]="0.6";
  cmd_args[9]="0.6";
  cmd_args[10]="0.6";
  cmd_args[11]="0.6";
  cmd_args[12]="0.6";
  cmd_args[13]="0.6";
  cmd_args[14]="0.6";
  modify->add_fix(n_args,cmd_args,lmp->suffix);

  // fix "m5" "all" "property/global" "coefficientRollingFriction" "peratomtypepair" "3" "0.15" "0.15" "0.15" "0.15" "0.15" "0.15" "0.15" "0.15" "0.15"
  n_args = 15;
  *cmd_args = new char[n_args];
  cmd_args[0]="m5";
  cmd_args[1]="all";
  cmd_args[2]="property/global";
  cmd_args[3]="coefficientRollingFriction";
  cmd_args[4]="peratomtypepair";
  cmd_args[5]="3";
  cmd_args[6]="0.15";
  cmd_args[7]="0.15";
  cmd_args[8]="0.15";
  cmd_args[9]="0.15";
  cmd_args[10]="0.15";
  cmd_args[11]="0.15";
  cmd_args[12]="0.15";
  cmd_args[13]="0.15";
  cmd_args[14]="0.15";
  modify->add_fix(n_args,cmd_args,lmp->suffix);

  // fix "m6" "all" "property/global" "cohesionEnergyDensity" "peratomtypepair" "3" "1200e3" "1200e3" "1200e3" "1200e3" "1200e3" "1200e3" "1200e3" "1200e3" "1200e3"
  n_args = 15;
  *cmd_args = new char[n_args];
  cmd_args[0]="m6";
  cmd_args[1]="all";
  cmd_args[2]="property/global";
  cmd_args[3]="cohesionEnergyDensity";
  cmd_args[4]="peratomtypepair";
  cmd_args[5]="3";
  cmd_args[6]="1200e3";
  cmd_args[7]="1200e3";
  cmd_args[8]="1200e3";
  cmd_args[9]="1200e3";
  cmd_args[10]="1200e3";
  cmd_args[11]="1200e3";
  cmd_args[12]="1200e3";
  cmd_args[13]="1200e3";
  cmd_args[14]="1200e3";
  modify->add_fix(n_args,cmd_args,lmp->suffix);

  // bond_style quartic
  n_args = 1;
  *cmd_args = new char[n_args];
  cmd_args[0]="quartic";
  if (atom->avec->bonds_allow == 0) error->all(FLERR,"Bond_style command when no bonds allowed");
  force->create_bond(cmd_args[0],lmp->suffix);
  if (force->bond) force->bond->settings(n_args-1,&cmd_args[1]);

  // pair_style gran model hertz tangential history cohesion sjkr rolling_friction epsd2 torsionTorque on
  n_args = 14;
  *cmd_args = new char[n_args];
  cmd_args[0]="hybrid/overlay";
  cmd_args[1]="gran";
  cmd_args[2]="model";
  cmd_args[3]="hertz";
  cmd_args[4]="tangential";
  cmd_args[5]="history";
  cmd_args[6]="cohesion";
  cmd_args[7]="sjkr";
  cmd_args[8]="rolling_friction";
  cmd_args[9]="epsd2";
  cmd_args[10]="torsionTorque";
  cmd_args[11]="on";
  cmd_args[12]="lj/cut";
  //cmd_args[13]=diametro;
  cmd_args[13]="20e-6";
  int num_remaining_arg = n_args - 1;
  char ** remaining_args = &cmd_args[1];
  force->create_pair(cmd_args[0], lmp->suffix);
  if (force->pair) force->pair->settings(num_remaining_arg,remaining_args);

  // pair_coeff * * lj/cut 1.0 1.0 2.5
  n_args = 5;
  *cmd_args = new char[n_args];
  cmd_args[0]="*";
  cmd_args[1]="*";
  cmd_args[2]="lj/cut";
  cmd_args[3]="1.0";
  cmd_args[4]="1.0";
  force->pair->coeff(n_args, cmd_args);

  // pair_coeff * * gran
  n_args = 3;
  *cmd_args = new char[n_args];
  cmd_args[0]="*";
  cmd_args[1]="*";
  cmd_args[2]="gran";
  force->pair->coeff(n_args, cmd_args);









  // fix gravi all gravity 9.81 vector 0.0 -1.0 0.0
  n_args = 8;
  *cmd_args = new char[n_args];
  cmd_args[0]="gravi";
  cmd_args[1]="all";
  cmd_args[2]="gravity";
  cmd_args[3]="9.81";
  cmd_args[4]="vector";
  cmd_args[5]="0.0";
  cmd_args[6]="-1.0";
  cmd_args[7]="0.0";
  modify->add_fix(n_args,cmd_args,lmp->suffix);

  // timestep 1e-6
  n_args = 1;
  *cmd_args = new char[n_args];
  cmd_args[0]="1e-4";
  update->dt = force->numeric(FLERR,cmd_args[0]);
  update->timestep_set = true;

  // region pedra mesh/tet file "+str(WorkDir)+"/meshes/"+str(vol_vtk)+" scale 1e-3 move 0 0 0 rotate 0 0 0 units box side out
  n_args = 18;
  *cmd_args = new char[n_args];
  cmd_args[0]="pedra";
  cmd_args[1]="mesh/tet";
  cmd_args[2]="file";
  cmd_args[3]="meshes/pedra.vtk";
  cmd_args[4]="scale";
  cmd_args[5]="1e-3";
  cmd_args[6]="move";
  cmd_args[7]="0";
  cmd_args[8]="0";
  cmd_args[9]="0";
  cmd_args[10]="rotate";
  cmd_args[11]="0";
  cmd_args[12]="0";
  cmd_args[13]="0";
  cmd_args[14]="units";
  cmd_args[15]="box";
  cmd_args[16]="side";
  cmd_args[17]="in";
  domain->add_region(n_args,cmd_args);

  /*
  // region domain block -1 +1 -1 +1 -1 +1 units box
  int iregion = domain->find_region("pedra");
  n_args = 10;
  *cmd_args = new char[n_args];
  cmd_args[0] = "bbox_pedra";
  cmd_args[1] = "block";
  cmd_args[2] = domain->regions[iregion]->extent_xlo;
  cmd_args[3] = domain->regions[iregion]->extent_xhi; 
  cmd_args[4] = domain->regions[iregion]->extent_ylo; 
  cmd_args[5] = domain->regions[iregion]->extent_yhi; 
  cmd_args[6] = domain->regions[iregion]->extent_zlo; 
  cmd_args[7] = domain->regions[iregion]->extent_zhi; 
  cmd_args[8] = "units";
  cmd_args[9] = "box";
  domain->add_region(n_args, cmd_args);
  */

  // lattice fcc 1.0 
  n_args = 2;
  *cmd_args = new char[n_args];
  //cmd_args[0] = "fcc";
  cmd_args[0] = "sc";
  cmd_args[1] = diametro; // Diametro da partícula.
  domain->set_lattice(n_args,cmd_args);

  // create_atoms	1 region pedra
  n_args = 3;
  *cmd_args = new char[n_args];
  cmd_args[0] = "1";
  cmd_args[1] = "region";
  cmd_args[2] = "pedra";
  command_creator<CreateAtoms>(lmp, n_args, cmd_args);

  // group group_pedra region pedra
  n_args = 3;
  *cmd_args = new char[n_args];
  cmd_args[0] = "groupPedra";
  cmd_args[1] = "region";
  cmd_args[2] = "pedra";
  group->assign(n_args,cmd_args);

  // set group groupPedra density 500 diameter 20e-3
  n_args = 6;
  *cmd_args = new char[n_args];
  cmd_args[0] = "group";
  cmd_args[1] = "groupPedra";
  cmd_args[2] = "density";
  cmd_args[3] = densidade;
  cmd_args[4] = "diameter";
  cmd_args[5] = diametro;
  command_creator<Set>(lmp, n_args, cmd_args);

  // thermo_style custom step dt atoms time tpcpu spcpu cpuremain
  n_args = 8;
  *cmd_args = new char[n_args];
  cmd_args[0]="custom";
  cmd_args[1]="step";
  cmd_args[2]="dt";
  cmd_args[3]="atoms";
  cmd_args[4]="time";
  cmd_args[5]="tpcpu";
  cmd_args[6]="spcpu";
  cmd_args[7]="cpuremain";
  output->create_thermo(n_args,cmd_args);

  // thermo 2000
  n_args = 1;
  *cmd_args = new char[n_args];
  cmd_args[0]="2000";
  output->set_thermo(n_args, cmd_args);

  /*
  // fix points01 all particletemplate/sphere 32452867 atom_type 1 density constant 3800 radius constant 20e-3
  n_args = 12;
  *cmd_args = new char[n_args];
  cmd_args[0]="points01";
  cmd_args[1]="all";
  cmd_args[2]="particletemplate/sphere";
  cmd_args[3]="32452867";
  cmd_args[4]="atom_type";
  cmd_args[5]="1";
  cmd_args[6]="density";
  cmd_args[7]="constant";
  cmd_args[8]="3800";
  cmd_args[9]="radius";
  cmd_args[10]="constant";
  cmd_args[11]="20e-3";
  modify->add_fix(n_args,cmd_args,lmp->suffix);
  */

  /*
  // fix partic01 all particledistribution/discrete 67867967 1 points01 1.00
  n_args = 7;
  *cmd_args = new char[n_args];
  cmd_args[0]="partic01";
  cmd_args[1]="all";
  cmd_args[2]="particledistribution/discrete";
  cmd_args[3]="67867967";
  cmd_args[4]="1";
  cmd_args[5]="points01";
  cmd_args[6]="1.00";
  modify->add_fix(n_args,cmd_args,lmp->suffix);
  */

  // fix zwall all wall/gran model hertz tangential history primitive type 1 zplane 0.0
  n_args = 12;
  cmd_args[1]="all";
  cmd_args[2]="wall/gran";
  cmd_args[3]="model";
  cmd_args[4]="hertz";
  cmd_args[5]="tangential";
  cmd_args[6]="history";
  cmd_args[7]="primitive";
  cmd_args[8]="type";
  cmd_args[9]="1";

  char *x[6][3] = {
    {"xwall0", "xplane", domain_x_lo},
    {"xwall1", "xplane", domain_x_hi},
    {"ywall0", "yplane", domain_y_lo},
    {"ywall1", "yplane", domain_y_hi},
    {"zwall0", "zplane", domain_z_lo},
    {"zwall1", "zplane", domain_z_hi}
  };

  for (int i = 0; i < 6; i++) 
  { 
    cmd_args[0] = x[i][0];
    cmd_args[10] = x[i][1];
    cmd_args[11] = x[i][2];
    std::cout << cmd_args[0] << " " << cmd_args[10] << " " << cmd_args[11] << " " << std::endl;
    modify->add_fix(n_args,cmd_args,lmp->suffix);
  } 

  //modify->add_fix(n_args,cmd_args,lmp->suffix);

  // run_style verlet
  n_args = 1;
  *cmd_args = new char[n_args];
  cmd_args[0]="verlet";
  update->create_integrate(n_args, cmd_args, lmp->suffix);

  // fix integr all nve/sphere
  n_args = 3;
  *cmd_args = new char[n_args];
  cmd_args[0]="integr";
  cmd_args[1]="all";
  cmd_args[2]="nve/sphere";
  modify->add_fix(n_args, cmd_args, lmp->suffix);

  /*
  // fix insert0 all insert/pack seed 49979687 distributiontemplate partic01 overlapcheck no vel constant 0.0 -10.0 0.0 region pedra volumefraction_region 1 ntry_mc 10000
  n_args = 17;
  *cmd_args = new char[n_args];
  cmd_args[0]="insert0";
  cmd_args[1]="all";
  cmd_args[2]="insert/pack";
  cmd_args[3]="seed";
  cmd_args[4]="49979687";
  cmd_args[5]="distributiontemplate";
  cmd_args[6]="partic01";
  cmd_args[7]="insert_every";
  cmd_args[8]="once";
  cmd_args[9]="overlapcheck";
  cmd_args[10]="yes";
  cmd_args[11]="volumefraction_region";
  cmd_args[12]="1.0";
  cmd_args[13]="region";
  cmd_args[14]="pedra";
  cmd_args[15]="ntry_mc";
  cmd_args[16]="10000";
  modify->add_fix(n_args,cmd_args,lmp->suffix);
  */

  //dump dump0 all custom/vtk 1000 post/dump*.vtk id type type x y z ix iy iz vx vy vz diameter
  n_args = 18;
  *cmd_args = new char[n_args];
  cmd_args[0]="dump0";
  cmd_args[1]="all";
  cmd_args[2]="custom/vtk";
  cmd_args[3]="1000";
  cmd_args[4]="post/dump*.vtk";
  cmd_args[5]="id";
  cmd_args[6]="type";
  cmd_args[7]="type";
  cmd_args[8]="x";
  cmd_args[9]="y";
  cmd_args[10]="z";
  cmd_args[11]="ix";
  cmd_args[12]="iy";
  cmd_args[13]="iz";
  cmd_args[14]="vx";
  cmd_args[15]="vy";
  cmd_args[16]="vz";
  cmd_args[17]="diameter";
  output->add_dump(n_args,cmd_args);

  // run 10000
  n_args = 1;
  *cmd_args = new char[n_args];
  cmd_args[0]="100000";
  command_creator<Run>(lmp, n_args, cmd_args);

  delete lammps;

  MPI_Finalize();
}
